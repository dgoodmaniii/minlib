/*
 * +AMDG
 */
/*
 * This document was begun on 14 May 1200, the feast of St.
 * Ubald, EC, and it is humbly dedicated to him and to the
 * Immaculate Heart of Mary for their prayers, and to the
 * Sacred Heart of Jesus for His mercy.
 */

#include<regex.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ncurses.h>
#include<menu.h>
#include"errcodes.h"
#include"utility.h"

/* stores our position in search results */
int ind = 0;

/* handles the full-text search functions, accessed by
 * gui.c; alters an array of ints containing the record
 * numbers of the matched records.  Returns 0 if matches
 * made, 1 if not, 2 if the regexp didn't compile.  Takes
 * the full database; the array for matches; and the regexp
 * string. */
int full_search(char **ptr, int **matched, char *pattern, char *err)
{
	regex_t comppat;
	int errornum = 0;
	int result;
	int i; int j = 0;
	int arrsize;
	int lastnum = -1;
	int currnum = -2;

	if ((*matched = realloc(*matched,1 * sizeof(int))) == NULL) {
		fprintf(stderr,"minlib:  insufficient memory for storing "
		"the numbers of matched records");
		exit(INSUFF_INTERNAL_MEMORY);
	}
	if ((errornum = regcomp(&comppat,pattern,REG_EXTENDED)) != 0) {
		regerror(errornum,&comppat,err,MAX_ERR_LENGTH);
		return -1;
	}
	arrsize = get_size(ptr) - 1;
	for (i = 0; i < arrsize; ++i) {
		result = regexec(&comppat,*(ptr+i),0,NULL,0);
		if (result != 0) {
			regerror(result,&comppat,err,MAX_ERR_LENGTH);
		} else {
			currnum = get_record_num(ptr,i) - 1;
			if (currnum != lastnum) {
				if ((*matched = realloc(*matched,(j+1)*sizeof(int)))==NULL) {
					fprintf(stderr,"minlib:  insufficient memory for storing "
					"the numbers of matched records");
					exit(INSUFF_INTERNAL_MEMORY);
				}
				*(*matched+(j++)) = currnum;
				lastnum = currnum;
			}
		}
	}
	regfree(&comppat);
	return j;
}
int field_search(char **ptr, int **matched, char *pattern, char *err)
{
	regex_t comppat;
	int errornum = 0;
	int result;
	int i; int j = 0;
	int arrsize;
	int lastnum = -1;
	int currnum;

	if ((*matched = realloc(*matched,1 * sizeof(int))) == NULL) {
		fprintf(stderr,"minlib:  insufficient memory for storing "
		"the numbers of matched records");
		exit(INSUFF_INTERNAL_MEMORY);
	}
	if ((errornum = regcomp(&comppat,pattern+2,REG_EXTENDED)) != 0) {
		regerror(errornum,&comppat,err,MAX_ERR_LENGTH);
		return -1;
	}
	arrsize = get_size(ptr) - 1;
	for (i = 0; i < arrsize; ++i) {
		if ((pattern[0] == 'a') && (strcmp(*(ptr+i),"AUTHOR")!=0)) {
			continue;
		} if ((pattern[0] == 't') && (strcmp(*(ptr+i),"TITLE")!=0)) {
			continue;
		} if ((pattern[0] == 'L') && (strcmp(*(ptr+i),"AUTHLAST")!=0)) {
			continue;
		} if ((pattern[0] == 'M') && (strcmp(*(ptr+i),"AUTHMID")!=0)) {
			continue;
		} if ((pattern[0] == 'F') && (strcmp(*(ptr+i),"AUTHFIRST")!=0)) {
			continue;
		} if ((pattern[0] == 'k') && (strcmp(*(ptr+i),"KEYWORDS")!=0)) {
			continue;
		} if ((pattern[0] == 'S') && (strcmp(*(ptr+i),"SUBJECT")!=0)) {
			continue;
		} if ((pattern[0] == 'p') && (strcmp(*(ptr+i),"PUBLISHER")!=0)) {
			continue;
		} if (pattern[0] == 'l') {
			if ((strcmp(*(ptr+i),"LANGUAGE")!=0) &&
				(strcmp(*(ptr+i),"LANGONE")!=0) &&
				(strcmp(*(ptr+i),"LANG")!=0)) {
				continue;
			}
		} if ((pattern[0] == 's') && (strcmp(*(ptr+i),"LANGTWO")!=0)) {
			continue;
		} if (pattern[0] == 'g') {
			if ((strcmp(*(ptr+i),"GENRE")!=0) &&
				(strcmp(*(ptr+i),"TYPE")!=0)) {
				continue;
			}
		} if ((pattern[0] == 'y') && (strcmp(*(ptr+i),"YEAR")!=0)) {
			continue;
		} if ((pattern[0] == 'D') && (strcmp(*(ptr+i),"DATE")!=0)) {
			continue;
		} if (pattern[0] == 'P') {
			if ((strcmp(*(ptr+i),"PLACE")!=0) &&
				(strcmp(*(ptr+i),"LOCATION")!=0)) {
				continue;
			}
		} if ((pattern[0] == 'e') && (strcmp(*(ptr+i),"EDITOR")!=0)) {
			continue;
		} if (pattern[0] == 'T') {
			if ((strcmp(*(ptr+i),"TRANS")!=0) &&
				(strcmp(*(ptr+i),"TRANSLATOR")!=0)) {
				continue;
			}
		} if ((pattern[0] == 'd') && (strcmp(*(ptr+i),"DIRECTOR")!=0)) {
			continue;
		} if ((pattern[0] == 'i') && (strcmp(*(ptr+i),"ILLUS.OR")!=0)) {
			continue;
		}
		result = regexec(&comppat,*(ptr+i+1),0,NULL,0);
		if (result != 0) {
			regerror(result,&comppat,err,MAX_ERR_LENGTH);
		} else {
			currnum = get_record_num(ptr,i) - 1;
			if (currnum != lastnum) {
				if ((*matched = realloc(*matched,(j+1)*sizeof(int)))==NULL) {
					fprintf(stderr,"minlib:  insufficient memory for storing "
					"the numbers of matched records");
					exit(INSUFF_INTERNAL_MEMORY);
				}
				*(*matched+(j++)) = currnum;
				lastnum = currnum;
			}
		}
	}
	regfree(&comppat);
	return j;
}
/* NOTE:  array *matched is free()d in gui.c if necessary,
 * because it's originally allocated there */

int proc_fsearch(MENU *menu, int matchnum, ITEM **lib_list, 
	int *matched, int *recnums, int numrecs, char action)
{
	int i = 0;
	int curr;

	curr = item_index(current_item(menu));
	if (action == 'n') {
		if (ind < (matchnum-1)) {
			ind += 1;
		} else {
			ind = 0;
		}
	} else if (action == 'N') {
		if (ind > 0) {
			ind -= 1;
		} else {
			ind = matchnum - 1;
		}
	} else if (action == 'f') {
		ind = 0;
	} else if (action == 'l') {
		ind = matchnum-1;
	}
	for (i = curr; (*(recnums+i) != *(matched+ind)); ++i) {
		if (i == numrecs)
			i = -1;
	}
	set_current_item(menu,lib_list[i]);
	return 0;
}

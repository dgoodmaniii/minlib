/*
 * +AMDG
 */
/*
 * This document was begun on 7 May 1200, the feast of St.
 * Stanislaus, EM, and it is humbly dedicated to him, to St.
 * Wulfric of Haselbury, and to the Immaculate Heart of Mary
 * for their prayers, and to the Sacred Heart of Jesus for
 * His mercy.
*/

#define _POSIX_SOURCE
#define _XOPEN_SOURCE 600

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include<locale.h>
#include<langinfo.h>
#include"options.h"
#include"errcodes.h"
#include"read_optfile.h"
#include"icon_type.h"
#include"readlib.h"
#include"gui.h"
#include"stats.h"
#include"utility.h"
#include"add_files.h"
#include"format_recs.h"

struct options *globopts;

int populate_recnums(int *recnums, int len);
int assign_to_opts(int ind);
int print_paths(char **ptr, int numrecs);

int main(int argc, char **argv)
{
	char *locale;
	locale = setlocale(LC_ALL,"");
	globopts = malloc((NUM_COLORS + NUM_APPS) * sizeof(struct options));
	assign_to_opts(TOP_FORE_COLOR);
	assign_to_opts(TOP_BACK_COLOR);
	assign_to_opts(BOT_FORE_COLOR);
	assign_to_opts(BOT_BACK_COLOR);
	assign_to_opts(MEN_FORE_COLOR);
	assign_to_opts(MEN_BACK_COLOR);
	assign_to_opts(DET_FIELD_FORE_COLOR);
	assign_to_opts(DET_FIELD_BACK_COLOR);
	assign_to_opts(DET_TXT_FORE_COLOR);
	assign_to_opts(DET_TXT_BACK_COLOR);
	assign_to_opts(DET_BACK_COLOR);
	assign_to_opts(PDF_VIEWER);
	assign_to_opts(HTML_VIEWER);
	assign_to_opts(EPUB_VIEWER);
	assign_to_opts(OGV_VIEWER);
	assign_to_opts(OGG_VIEWER);
	assign_to_opts(DVI_VIEWER);
	assign_to_opts(PS_VIEWER);
	assign_to_opts(OFFICE_VIEWER);
	assign_to_opts(MAIL_SENDER);
	assign_to_opts(MP3_VIEWER);
	assign_to_opts(IMG_VIEWER);
	char **ptr = NULL; char **formlist; int *recnums;
	int numlines = 0;
	int numrecs = 0;
	int i = 0;
	char c;
	char defform[] = "%30t | %24a | %8l | %9g";
	char *formstring = NULL;
	int didconfigfile = 1;
	int add_succ;
	char **args;
	int printval = 0;
	int statsval = 0;
	int pathval = 0;
	int appval = 0;
	char *appdir = NULL;
	int numfiles = 0;
	char **fn = NULL;
	char *home;
	char *confdir = NULL;
	char *conffn = NULL;
	char specdir[] = "/.config/minlib/";

	args = argv;
	opterr = 0;
	home = getenv("HOME");
	if (home != NULL) {
		if ((confdir = malloc((strlen(home) + strlen(specdir) + 3)
						* sizeof(char))) == NULL) {
			fprintf(stderr,"minlib:  error:  insufficient "
					"internal memory\n");
			exit(INSUFF_INTERNAL_MEMORY);
		}
	}
	strcpy(confdir,home); strcat(confdir,specdir);
	while ((c = getopt(argc,argv,"VPsptf:r:c:a:A:")) != -1) {
		switch (c) {
		case 'V':
			printf("minlib v1.4\n");
			printf("Copyright (C) 2014  Donald P. Goodman III\n");
			printf("License GPLv3+:  GNU GPL version 3 or "
			"later <http://gnu.org/licenses/gpl.html>\n");
			printf("This is free software:  you are free "
			"to change and redistribute it.  There is NO "
			"WARRANTY, to the extent permitted by law.\n");
			exit(ALLGOOD);
			break;
		case 'a':
			add_succ = add_files(optarg,appval,ptr);
			exit(add_succ);
			break;
		case 'A':
			if ((appdir = malloc(strlen(optarg) + 1 * sizeof(char))) == NULL) {
				fprintf(stderr,"minlib:  error:  insufficient memory\n");
				exit(INSUFF_INTERNAL_MEMORY);
			}
			strncpy(appdir,optarg,strlen(optarg)+1);
			appval = 1;
			break;
		case 'P':
			pathval = 1;
			break;
		case 't':
			break;
		case 'r':
			if (formstring == NULL) {
				if ((formstring = malloc(1*sizeof(char)))==NULL) {
					fprintf(stderr,"minlib:  insufficient memory for "
					"format string\n");
					exit(INSUFF_MEMORY_FORMSTRING);
				}
				*formstring = '\0';
			}
			if (strlen(optarg) > strlen(formstring)) {
				if ((formstring = realloc(formstring,
				(strlen(optarg)+1)*sizeof(char)))==NULL) {
					fprintf(stderr,"minlib:  insufficient memory for "
					"format string specified with -r\n");
					exit(INSUFF_MEMORY_FORMSTRING);
				}
			}
			strcpy(formstring,optarg);
			break;
		case 'f':
			if (fn == NULL) {
				fn = malloc(sizeof(char *));
			} else {
				fn = realloc(fn,sizeof(char *) * (numfiles + 1));
			}
			if ((*(fn + numfiles) = malloc(sizeof(char) * (strlen(optarg)
								+ strlen(confdir) + 4))) == NULL) {
				fprintf(stderr,"minlib:  insufficient memory for "
						"the filename \"%s\"\n",*(fn+numfiles));
				exit(INSUFF_MEMORY_FILENAME);
			}
			if (file_exist(confdir,optarg) == 1) {
				if (file_exist("",optarg) == 1) {
					fprintf(stderr,"minlib:  error:  file "
							"\"%s\" does not exist; skipping...\n",optarg);
					break;
				} else {
					strcpy(*(fn+numfiles),optarg);
				}
			} else {
				strcpy(*(fn+numfiles),confdir);
				strcat(*(fn+numfiles),optarg);
			}
			++numfiles;
			break;
		case 'c':
			if (conffn == NULL) {
				if ((conffn = malloc(strlen(optarg) +
								strlen(confdir) + 4)) == NULL) {
					fprintf(stderr,"minlib:  error:  insufficient "
							"internal memory\n");
					exit(INSUFF_INTERNAL_MEMORY);
				}
			}
			if (strcmp(optarg,"-") != 0) {
				if (file_exist(confdir,optarg) == 1) {
					if (file_exist("",optarg) == 1) {
						fprintf(stderr,"minlib:  error:  file "
								"\"%s\" does not exist; skipping...\n",optarg);
						break;
					} else {
						strcpy(conffn,optarg);
					}
				} else {
					strcpy(conffn,confdir);
					strcat(conffn,optarg);
				}
			} else {
				strcpy(conffn,optarg);
			}
			didconfigfile=read_optfile(&fn,&formstring,conffn,&numfiles,
					&globopts);
			break;
		case 's':
			statsval = 1;
			break;
		case 'p':
			printval = 1;
			break;
		case '?':
			if ((optopt == 'f') || (optopt == 'r') || (optopt == 'c')
			|| (optopt == 'a') || (optopt == 'A')) {
				fprintf(stderr,"minlib:  option \"%c\" requires "
				"an argument\n",optopt);
				exit(NEED_ARGUMENT_ARG);
			}
			fprintf(stderr,"minlib:  unrecognized option \"%c\"\n",optopt);
			exit(BAD_OPTION);
			break;
		}
	}
	if (didconfigfile == 1) {
		read_optfile(&fn,&formstring,NULL,&numfiles,&globopts);
	}
	if (formstring == NULL) {
		if ((formstring = malloc((strlen(defform)+1)*sizeof(char)))==NULL) {
			fprintf(stderr,"minlib:  insufficient memory for "
			"default format string\n");
			exit(INSUFF_MEMORY_FORMSTRING);
		}
		strcpy(formstring,defform);
	}
	for (i = 0; i < numfiles; ++i) {
		numlines += count_lines_file(*(fn+i));
	}
	if (numlines == 0) {
		fprintf(stderr,"minlib:  error:  no input file was "
				"specified\n");
		exit(NO_DATA_FILE);
	}
	ptr = malloc(sizeof(char *) * ((numlines + 1) * 2));
	numrecs = fill_db(ptr,fn,numfiles);
	if ((formlist = malloc((numrecs+1) * sizeof(char*))) == NULL) {
		fprintf(stderr,"minlib:  insufficient memory to load "
		"%d summarized records from files\n",numrecs);
		exit(INSUFF_MEMORY_FORMAT);
	}
	*(formlist+numrecs) = NULL;
	if ((recnums = malloc((numrecs+1) * sizeof(int))) == NULL) {
		fprintf(stderr,"minlib:  insufficient memory to track "
		"%d summarized records from files\n",numrecs);
		exit(INSUFF_MEMORY_TRACK);
	}
	populate_recnums(recnums,numrecs+1);
	for (i = 0; i < numrecs; ++i) {
		*(formlist+i) = malloc(MAX_FORMAT_STR_LEN * sizeof(char) + 1);
		*(*(formlist+i+0)) = '\0';
	}
	if (appval == 1) {
		add_succ = add_files(appdir,appval,ptr);
		free(appdir);
		exit(ALLGOOD);
	}
	build_icontypes();
	format_recs(ptr,formstring,formlist,numrecs,recnums);
	if (printval == 1) {
		stats_report(recnums,ptr,formlist,numrecs,statsval);
		exit(ALLGOOD);
	}
	if (pathval == 1) {
		print_paths(ptr,numrecs);
		exit(ALLGOOD);
	}
	if (load_gui(ptr,formlist,recnums,numrecs,globopts) == 1) {
		for (i = 0; i < argc; ++i)
			printf("\t%s\n",args[i]);
		execv(args[0],args);
	}
	free_db(ptr,numlines+1*2);
	free(ptr);
	for (i=0; i <= numrecs; ++i)
		free(*(formlist+i));
	free(formlist);
	free(recnums);
	free(formstring);
	for (i = 0; i < (NUM_COLORS + NUM_APPS); ++i)
		free((globopts+i)->optval);
	free(globopts);
	for (i = 0; i < numfiles; ++i)
		free(*(fn+i));
	free(fn);
	if (confdir != NULL)
		free(confdir);
	if (conffn != NULL)
		free(conffn);
	free_iconlist();
	return 0;
}

int populate_recnums(int *recnums, int len)
{
	int i;
	
	for (i = 0; i < len; ++i)
		*(recnums+i) = i;
	return 0;
}

int assign_to_opts(int ind)
{
	(globopts+ind)->optcode = ind;
	(globopts+ind)->optval = "";
	return 0;
}

int print_paths(char **ptr, int numrecs)
{
	int i;

	for (i = 0; *(ptr+i) != NULL; ++i) {
		if (strstr(*(ptr+i),"PATH")) {
			if (access(*(ptr+i+1), F_OK) != 0) {
				fprintf(stderr,"minlib:  error:  file \"%s\" "
						"does not appear to exist\n",*(ptr+i+1));
			} else {
				printf("%s\n",*(ptr+i+1));
			}
		}
	}
	return 0;
}

/*
 * +AMDG
 */
/*
 * This document was begun on 10 May 1200, the feast of Ss.
 * Nereus, Achilleus, Domitilla V, and Pancras, MM, and it
 * is humbly dedicated to them and to the Immaculate Heart
 * of Mary for their prayers and to the Sacred Heart of
 * Jesus for His mercy.
 */

/* Loads the ncurses GUI interface for minlib. */

#define _POSIX_C_SOURCE 200809

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<ncurses.h>
#include<menu.h>
#include"errcodes.h"
#include"options.h"
#include"full_search.h"
#include"utility.h"

extern struct options globopts[];

int open_path(char *s, struct options *globopts);
int send_path(char *s, struct options *globopts, char **ptr, int sel);
int get_number();
int get_col_int(char *s);
int menu_help_scr();
int shell_out(char *s);
int clean_bottom_line(int row, int col);
int set_search_line(int row, int col, char *s, int matchnum);
int set_comm_open(int row, int col);
int set_comm_mail(int row, int col);
int set_bot_line_match(MENU *lib_menu, int row, int col, char *s);
int set_fullsearch_buffer(MENU *lib_menu,char*s,int row, int col);
int set_pattern_buffer(MENU *lib_menu,char *s, int row, int col);
int frame_menu_help(int row, int col);
int frame_detail_screen(int row, int col, int recordnum);
int frame_main_screen(int numrecs, int row, int col);
int print_top_menu_help(WINDOW *win, int row, int col);
int print_bot_menu_help(WINDOW *win, int row, int col);
int print_top_details(WINDOW *win, int row, int col, int recordnum);
int print_bot_details(WINDOW *win, int row, int col);
int print_top_line(WINDOW *win, int row, int col, int numrecs);
int print_bottom_open(char *s);
int print_bot_line(WINDOW *win, int row, int col);
int print_center(WINDOW *win, int row, char *s);
int print_right(WINDOW *win, int row, char *s);
int highlight_line(WINDOW *win, int row, int col);
int wrap_print(WINDOW *win,char *s, int cols, int row);
int initialize_colors(struct options *globopts);
int display_details(char **ptr,int *recnums,int sel_rec,int row,
	int col,struct options *globopts);
int print_generic_bottom_line(char *s);

int load_gui(char **ptr, char **formlist, int *recnums, int numrecs,
struct options *globopts)
{
	int i; int c;
	ITEM **lib_list;
	MENU *lib_menu;
	WINDOW *lib_menu_win;
	int row, col;
	char buf[12] = "";
	char pattern[MAX_REGEXP_LEN+1];
	int sel_rec;
	char regexperror[MAX_ERR_LENGTH+1];
	int matchnum = 0;
	int *matched;
	int doreload = 0;

	if ((matched = malloc(1 * sizeof(int))) == NULL) {
		fprintf(stderr,"minlib:  insufficient memory to store "
		"the array of matched strings in a full search");
		exit(INSUFF_INTERNAL_MEMORY);
	}
	if (!freopen("/dev/tty","r",stdin)) {
		perror("/dev/tty");
		exit(1);
	}
	initscr();
	cbreak();
	noecho();
	keypad(stdscr,TRUE);
	curs_set(0);
	initialize_colors(globopts);
	getmaxyx(stdscr,row,col);
	frame_main_screen(numrecs, row, col);
	lib_list = (ITEM **)calloc(numrecs+1, sizeof(ITEM *));
	for (i = 0; i < numrecs; ++i)
		lib_list[i] = new_item(*(formlist+i),"");
	lib_list[numrecs] = (ITEM *)NULL;
	lib_menu = new_menu((ITEM **)lib_list);
	lib_menu_win = newwin(row-3,col,1,0);
	keypad(lib_menu_win,TRUE);
	set_menu_win(lib_menu,lib_menu_win);
	set_menu_sub(lib_menu,derwin(lib_menu_win,0,0,0,0));
	set_menu_format(lib_menu,row-3,1);
	set_menu_mark(lib_menu,"");
	set_menu_fore(lib_menu,COLOR_PAIR(3) | A_REVERSE);
	set_menu_back(lib_menu,COLOR_PAIR(3));
	refresh();
	post_menu(lib_menu);
	wrefresh(lib_menu_win);
	refresh();
	while ((c = wgetch(lib_menu_win)) != 'q') {
		switch (c) {
		case KEY_DOWN: case 'j':
			menu_driver(lib_menu, REQ_DOWN_ITEM);
			break;
		case KEY_UP: case 'k':
			menu_driver(lib_menu, REQ_UP_ITEM);
			break;
		case KEY_PPAGE: case '-':
			menu_driver(lib_menu, REQ_SCR_UPAGE);
			break;
		case KEY_NPAGE: case ' ':
			menu_driver(lib_menu, REQ_SCR_DPAGE);
			break;
		case KEY_HOME: case 'F':
			menu_driver(lib_menu, REQ_FIRST_ITEM);
			break;
		case KEY_END: case 'L':
			menu_driver(lib_menu, REQ_LAST_ITEM);
			break;
		case 'p':
			if (strcmp(buf,"")) {
				set_bot_line_match(lib_menu,row,col,buf);
				menu_driver(lib_menu, REQ_NEXT_MATCH);
			}
			break;
		case 'P':
			if (strcmp(buf,"")) {
				set_bot_line_match(lib_menu,row,col,buf);
				menu_driver(lib_menu, REQ_PREV_MATCH);
			}
			break;
		case 'm':
			set_pattern_buffer(lib_menu,buf,row,col);
			set_menu_pattern(lib_menu,buf);
			break;
		case 'n':
			if (matchnum > 0) {
				proc_fsearch(lib_menu,matchnum,lib_list,matched,recnums,
					numrecs,'n');
				set_search_line(row, col, pattern, matchnum);
			} else {
				menu_driver(lib_menu, REQ_DOWN_ITEM);
			}
			break;
		case 'N':
			if (matchnum > 0) {
				proc_fsearch(lib_menu,matchnum,lib_list,matched,recnums,
					numrecs,'N');
				set_search_line(row, col, pattern, matchnum);
			} else {
				menu_driver(lib_menu, REQ_UP_ITEM);
			}
			break;
		case 'f':
			proc_fsearch(lib_menu,matchnum,lib_list,matched,recnums,
				numrecs,'f');
			break;
		case 'l':
			proc_fsearch(lib_menu,matchnum,lib_list,matched,recnums,
				numrecs,'l');
			break;
		case '/':
			if ((matched = realloc(matched,(1 * sizeof(int)))) == NULL) {
				fprintf(stderr,"minlib:  insufficient memory to store "
				"the array of matched strings in a full search");
				exit(INSUFF_INTERNAL_MEMORY);
			}
			set_fullsearch_buffer(lib_menu,pattern,row,col);
			if ((pattern[0] == '~') && (pattern[1] != '\0') &&
					(pattern[2] == '~'))
				matchnum = field_search(ptr,&matched,pattern+1,regexperror);
			else
				matchnum = full_search(ptr,&matched,pattern,regexperror);
			if (matchnum > 0) {
				proc_fsearch(lib_menu,matchnum,lib_list,matched,recnums,
					numrecs,'f');
				set_search_line(row, col, pattern, matchnum);
			} else {
				set_search_line(row, col, regexperror, matchnum);
			}
			break;
		case ':':
			shell_out(NULL);
			break;
		case 'r':
			doreload = 1;
			goto cleanup;
			break;
		case 'h':
			menu_help_scr();
			menu_driver(lib_menu, REQ_DOWN_ITEM);
			menu_driver(lib_menu, REQ_UP_ITEM);
			frame_main_screen(numrecs, row, col);
			post_menu(lib_menu);
			wrefresh(lib_menu_win);
			clean_bottom_line(row,col);
			refresh();
			break;
		case 10: /* ENTER */
			sel_rec = item_index(current_item(lib_menu));
			unpost_menu(lib_menu);
			display_details(ptr,recnums,sel_rec,row,col,globopts);
			frame_main_screen(numrecs, row, col);
			post_menu(lib_menu);
			wrefresh(lib_menu_win);
			clean_bottom_line(row,col);
			refresh();
			break;
		}
		wrefresh(lib_menu_win);
	}
	cleanup:
	unpost_menu(lib_menu);
	free_menu(lib_menu);
	for (i = 0; i <= numrecs; ++i)
		free_item(lib_list[i]);
	free(lib_list);
	free(matched);
	endwin();
	if (doreload == 1)
		return 1;
	return 0;
}

int menu_help_scr()
{
	WINDOW *menu_help_win;
	int row, col;
	char c;
	int x, y = 0;
	int i;

	getmaxyx(stdscr,row,col);
	werase(stdscr); refresh();
	frame_menu_help(row, col); refresh();
	menu_help_win = newwin(row-3,col,1,0);
	wrefresh(menu_help_win); refresh();
	keypad(menu_help_win,TRUE);

	y = 1;
	wbkgd(menu_help_win,COLOR_PAIR(6));
	wattron(menu_help_win,COLOR_PAIR(4));
	mvwprintw(menu_help_win,y,2,"↑,k\t");
	wattroff(menu_help_win,COLOR_PAIR(4));
	wattron(menu_help_win,COLOR_PAIR(5));
	mvwprintw(menu_help_win,y,8,"up a menu item");

	y += 2;
	wbkgd(menu_help_win,COLOR_PAIR(6));
	wattron(menu_help_win,COLOR_PAIR(4));
	mvwprintw(menu_help_win,y,2,"↓,j\t");
	wattroff(menu_help_win,COLOR_PAIR(4));
	wattron(menu_help_win,COLOR_PAIR(5));
	mvwprintw(menu_help_win,y,8,"down a menu item");

	y += 2;
	wbkgd(menu_help_win,COLOR_PAIR(6));
	wattron(menu_help_win,COLOR_PAIR(4));
	mvwprintw(menu_help_win,y,2,"PgUp/-\t");
	wattroff(menu_help_win,COLOR_PAIR(4));
	wattron(menu_help_win,COLOR_PAIR(5));
	mvwprintw(menu_help_win,y,12,"Up a page of menu items");

	y += 2;
	wbkgd(menu_help_win,COLOR_PAIR(6));
	wattron(menu_help_win,COLOR_PAIR(4));
	mvwprintw(menu_help_win,y,2,"PgDn/Sp\t");
	wattroff(menu_help_win,COLOR_PAIR(4));
	wattron(menu_help_win,COLOR_PAIR(5));
	mvwprintw(menu_help_win,y,12,"Down a page of menu items");

	y += 2;
	wbkgd(menu_help_win,COLOR_PAIR(6));
	wattron(menu_help_win,COLOR_PAIR(4));
	mvwprintw(menu_help_win,y,2,"F\t");
	wattroff(menu_help_win,COLOR_PAIR(4));
	wattron(menu_help_win,COLOR_PAIR(5));
	mvwprintw(menu_help_win,y,8,"First menu item");

	y += 2;
	wbkgd(menu_help_win,COLOR_PAIR(6));
	wattron(menu_help_win,COLOR_PAIR(4));
	mvwprintw(menu_help_win,y,2,"L\t");
	wattroff(menu_help_win,COLOR_PAIR(4));
	wattron(menu_help_win,COLOR_PAIR(5));
	mvwprintw(menu_help_win,y,8,"Last menu item");

	y += 2;
	wbkgd(menu_help_win,COLOR_PAIR(6));
	wattron(menu_help_win,COLOR_PAIR(4));
	mvwprintw(menu_help_win,y,2,"m\t");
	wattroff(menu_help_win,COLOR_PAIR(4));
	wattron(menu_help_win,COLOR_PAIR(5));
	mvwprintw(menu_help_win,y,8,"Simple character match");

	y += 2;
	wbkgd(menu_help_win,COLOR_PAIR(6));
	wattron(menu_help_win,COLOR_PAIR(4));
	mvwprintw(menu_help_win,y,4,"p\t");
	wattroff(menu_help_win,COLOR_PAIR(4));
	wattron(menu_help_win,COLOR_PAIR(5));
	mvwprintw(menu_help_win,y,8,"Next match");

	y += 2;
	wbkgd(menu_help_win,COLOR_PAIR(6));
	wattron(menu_help_win,COLOR_PAIR(4));
	mvwprintw(menu_help_win,y,4,"P\t");
	wattroff(menu_help_win,COLOR_PAIR(4));
	wattron(menu_help_win,COLOR_PAIR(5));
	mvwprintw(menu_help_win,y,8,"Previous match");

	y = 1; x = col/2 + 2;
	wbkgd(menu_help_win,COLOR_PAIR(6));
	wattron(menu_help_win,COLOR_PAIR(4));
	mvwprintw(menu_help_win,y,x,"/\t");
	wattroff(menu_help_win,COLOR_PAIR(4));
	wattron(menu_help_win,COLOR_PAIR(5));
	mvwprintw(menu_help_win,y,x+8,"Full regex match");

	y += 2; x = col/2 + 2;
	wbkgd(menu_help_win,COLOR_PAIR(6));
	wattron(menu_help_win,COLOR_PAIR(4));
	mvwprintw(menu_help_win,y,x+2,"n\t");
	wattroff(menu_help_win,COLOR_PAIR(4));
	wattron(menu_help_win,COLOR_PAIR(5));
	mvwprintw(menu_help_win,y,x+8,"Next regex match");

	y += 2; x = col/2 + 2;
	wbkgd(menu_help_win,COLOR_PAIR(6));
	wattron(menu_help_win,COLOR_PAIR(4));
	mvwprintw(menu_help_win,y,x+2,"N\t");
	wattroff(menu_help_win,COLOR_PAIR(4));
	wattron(menu_help_win,COLOR_PAIR(5));
	mvwprintw(menu_help_win,y,x+8,"Previous regex match");

	y += 2; x = col/2 + 2;
	wbkgd(menu_help_win,COLOR_PAIR(6));
	wattron(menu_help_win,COLOR_PAIR(4));
	mvwprintw(menu_help_win,y,x+2,"~?~\t");
	wattroff(menu_help_win,COLOR_PAIR(4));
	wattron(menu_help_win,COLOR_PAIR(5));
	mvwprintw(menu_help_win,y,x+8,"Field search; replace \"?\"");
	mvwprintw(menu_help_win,y+1,x+8,"appropriate field letter:");
	mvwprintw(menu_help_win,y+2,x+8,"t:title, a:author/artist,");
	mvwprintw(menu_help_win,y+3,x+8,"p:publisher, k:keywords,");
	mvwprintw(menu_help_win,y+4,x+8,"S:subject, l:language,");
	mvwprintw(menu_help_win,y+5,x+8,"g:genre, y:year, d:date,");
	mvwprintw(menu_help_win,y+6,x+8,"m:medium, P:place, e:editor,");
	mvwprintw(menu_help_win,y+7,x+8,"T:translator, d:director,");
	mvwprintw(menu_help_win,y+8,x+8,"i:illustrator");

	y += 10; x = col/2 + 2;
	wbkgd(menu_help_win,COLOR_PAIR(6));
	wattron(menu_help_win,COLOR_PAIR(4));
	mvwprintw(menu_help_win,y,x,"ENT\t");
	wattroff(menu_help_win,COLOR_PAIR(4));
	wattron(menu_help_win,COLOR_PAIR(5));
	mvwprintw(menu_help_win,y,x+8,"Select menu item");

	for (i = 0; i < row-2; ++i) {
		mvwprintw(menu_help_win,i,col/2,"|");
	}

	wrefresh(menu_help_win); refresh();
	if ((c = wgetch(menu_help_win)) == 'q') {
		werase(menu_help_win);
		delwin(menu_help_win);
		refresh();
	}
	return 0;
}

int shell_out(char *s)
{
	const char *name = "SHELL";
	char *shname;
	char *defshell = "/bin/sh";

	def_prog_mode();
	endwin();
	if (s != NULL) {
		chdir(s);
		fprintf(stderr,"%s\n",s);
	}
	shname = getenv(name);
	if (shname == NULL) {
		if ((shname = malloc((strlen(defshell)+1) * sizeof(char))) == NULL) {
			fprintf(stderr,"minlib:  insufficient memory to "
			"store the name of the default shell, \"%s\"\n",defshell);
			exit(INSUFF_INTERNAL_MEMORY);
		}
		strcat(shname,defshell);
	}
	system(shname);
	reset_prog_mode();
	refresh();
	return 0;
}

int display_details(char **ptr,int *recnums,int sel_rec,int row,
int col,struct options *globopts)
{
	WINDOW *sel_item_win;
	int i; int j;
	int k = 0; /* track whether it's a field title or not */
	int d;
	int wrapped = 0;
	int lines_rec;
	char *firstpath = NULL;
	char **paths;
	int np = 0;
	int cp = 0;
	int opennum;
	int covernum = -1;
	int n = 0;
	int currnum = 0;

	if ((paths = malloc(1 * sizeof(char *))) == NULL) {
		fprintf(stderr,"minlib:  error:  insufficient memory\n");
		exit(INSUFF_INTERNAL_MEMORY);
	}
	werase(stdscr);
	frame_detail_screen(row, col, *(recnums+sel_rec)+1); refresh();
	for (i = 0; *(ptr+i) != NULL; ++i) {
		if ((atoi(*(ptr+i)) == (*(recnums+sel_rec)+1)) &&
			strstr(*(ptr+i),"%%")) {
			break;
		}
	}
	n = i+1;
	while ((*(ptr+n) != NULL) && (!strstr(*(ptr+n),"%%"))) {
		++n;
	}
	currnum = i;
	n -= i; n *= 2;
	sel_item_win = newpad((n),col); /* FIXME */
	keypad(sel_item_win,TRUE);
	wbkgd(sel_item_win,COLOR_PAIR(6));
	for (i=i+1,j=2; *(ptr+i) != NULL && !strstr(*(ptr+i),"%%"); ++i) {
		if ((strstr(*(ptr+i),"PATH")) ||
				(strstr(*(ptr+i),"COVER"))) {
			if (strstr(*(ptr+i),"COVER")) {
				covernum = np;
			}
			if (np == 0) {
				if ((firstpath=malloc((strlen(*(ptr+i+1))+1)*sizeof(char)))==NULL){
					fprintf(stderr,"minlib:  error:  insufficient memory");
					exit(INSUFF_INTERNAL_MEMORY);
				}
				strcpy(firstpath,*(ptr+i+1));
				stripfilename(firstpath);
			}
			if ((paths = realloc(paths,(np+1) * sizeof(char *))) == NULL) {
				fprintf(stderr,"minlib:  error:  insufficient memory\n");
				exit(INSUFF_INTERNAL_MEMORY);
			}
			if ((*(paths+np) = malloc((strlen(*(ptr+i+1)) + 1) *
							sizeof(char))) == NULL) {
				fprintf(stderr,"minlib:  error:  insufficient memory");
				exit(INSUFF_INTERNAL_MEMORY);
			}
			strcpy(*(paths+np),*(ptr+i+1));
			++np;
		}
		if (k == 0) {
			++k;
			if ((!strcmp(*(ptr+i),"PATH")) || (!strcmp(*(ptr+i),"COVER"))) {
				wattron(sel_item_win,COLOR_PAIR(4));
				mvwprintw(sel_item_win,j,2,"%4d|%.14s:",cp++,*(ptr+i));
				wattroff(sel_item_win,COLOR_PAIR(4));
			} else {
				wattron(sel_item_win,COLOR_PAIR(4));
				mvwprintw(sel_item_win,j,2,"%.18s:",*(ptr+i));
				wattroff(sel_item_win,COLOR_PAIR(4));
			}
		} else {
			wattron(sel_item_win,COLOR_PAIR(5));
			wrapped = wrap_print(sel_item_win,*(ptr+i),col,j);
			wresize(sel_item_win,n+wrapped,col);
			wattroff(sel_item_win,COLOR_PAIR(5));
			j += (wrapped + 1);
			k = 0;
		}
		lines_rec = j - row + 4;
		prefresh(sel_item_win,0,0,1,0,row-3,col-1);
	}
	i = 0;
	while ((d = wgetch(sel_item_win)) != 'q') {
		switch(d) {
		case KEY_DOWN: case 'j':
			if (i < lines_rec)
				++i;
			frame_detail_screen(row, col, *(recnums+sel_rec)+1); refresh();
			prefresh(sel_item_win,i,0,1,0,row-3,col-1);
			wrefresh(sel_item_win);
			refresh();
			break;
		case KEY_UP: case 'k':
			if (i > 1)
				--i;
			frame_detail_screen(row, col, *(recnums+sel_rec)+1); refresh();
			prefresh(sel_item_win,i,0,1,0,row-3,col-1);
			wrefresh(sel_item_win);
			refresh();
			break;
		case ' ': case KEY_NPAGE:
			if (i < lines_rec) {
				i += (row - 1);
				if (i > lines_rec)
					i = lines_rec;
			}
			frame_detail_screen(row, col, *(recnums+sel_rec)+1); refresh();
			prefresh(sel_item_win,i,0,1,0,row-3,col-1);
			wrefresh(sel_item_win);
			refresh();
			break;
		case '-': case KEY_PPAGE:
			i -= (row-1);
			if (i < 1)
				i = 1;
			frame_detail_screen(row, col, *(recnums+sel_rec)+1); refresh();
			prefresh(sel_item_win,i,0,1,0,row-3,col-1);
			wrefresh(sel_item_win);
			refresh();
			break;
		case 's':
			getmaxyx(stdscr,row,col);
			highlight_line(stdscr,row-2,col);
			set_comm_mail(row, col);
			opennum = get_number();
			if (opennum < np) {
				send_path(*(paths+opennum),globopts,ptr,currnum);
			} else {
				print_generic_bottom_line("Error:  no such path");
			}
			clear();
			frame_detail_screen(row, col, *(recnums+sel_rec)+1); refresh();
			prefresh(sel_item_win,i,0,1,0,row-3,col-1);
			wrefresh(sel_item_win);
			refresh();
			break;
		case 'c':
			if (covernum < 0) {
				print_generic_bottom_line("Error:  no cover "
						"defined for this title");
				refresh();
			} else {
				getmaxyx(stdscr,row,col);
				highlight_line(stdscr,row-2,col);
				if ((open_path(*(paths+covernum),globopts)) != 0) {
					print_generic_bottom_line("Error:  don't know "
							"how to open that cover");
				} else {
					print_bottom_open(*(paths+covernum));
				}
				frame_detail_screen(row, col, *(recnums+sel_rec)+1); refresh();
			}
			break;
		case 'o':
			getmaxyx(stdscr,row,col);
			highlight_line(stdscr,row-2,col);
			set_comm_open(row, col);
			opennum = get_number();
			if (opennum < np) {
				if ((open_path(*(paths+opennum),globopts)) != 0) {
					print_generic_bottom_line("Error:  don't know "
							"how to open that path");
				} else {
					print_bottom_open(*(paths+opennum));
				}
			} else {
				print_generic_bottom_line("Error:  no such path");
			}
			frame_detail_screen(row, col, *(recnums+sel_rec)+1); refresh();
			break;
		case ':':
			shell_out(firstpath);
			break;
		}
	}
	if (firstpath != NULL)
		free(firstpath);
	werase(sel_item_win);
	wrefresh(sel_item_win);
	delwin(sel_item_win);
	for (i = 0; i < np; ++i)
		free(*(paths+i));
	free(paths);
	return 0;
}

int clean_bottom_line(int row, int col)
{
	int i;
	for (i = 0; i < col; ++i) {
		mvprintw(row-1,i," ");
		refresh();
	}
	return 0;
}

int set_search_line(int row, int col, char *s, int matchnum)
{
	clean_bottom_line(row,col); refresh();
	if (matchnum == 0) {
		mvwprintw(stdscr,row-1,0,"%s",s);
		attron(A_BOLD);
		mvwprintw(stdscr,row-1,strlen(s)+4,"(not found)");
		attroff(A_BOLD);
	} else if (matchnum > 0) {
		mvwprintw(stdscr,row-1,0,"/%s",s);
		attron(A_BOLD);
		mvwprintw(stdscr,row-1,strlen(s)+4,"(%d) matches",matchnum);
		attroff(A_BOLD);
	} else {
		attron(A_BOLD);
		mvwprintw(stdscr,row-1,0,"error:  %s",s);
		attroff(A_BOLD);
	}
	refresh();
	return 0;
}

int set_comm_mail(int row, int col)
{
	attron(A_REVERSE | A_BOLD);
	attron(COLOR_PAIR(2));
	clean_bottom_line(row-1,col); refresh();
	mvwprintw(stdscr,row-2,0,"MAIL:  ");
	mvwprintw(stdscr,row-2,6,"File Number to Attach (hit enter)");
	attroff(COLOR_PAIR(2));
	attroff(A_REVERSE | A_BOLD);
	refresh();
	return 0;
}

int set_comm_open(int row, int col)
{
	attron(A_REVERSE | A_BOLD);
	attron(COLOR_PAIR(2));
	clean_bottom_line(row-1,col); refresh();
	mvwprintw(stdscr,row-2,0,"OPEN:  ");
	mvwprintw(stdscr,row-2,6,"File Number to Open (hit enter)");
	attroff(COLOR_PAIR(2));
	attroff(A_REVERSE | A_BOLD);
	refresh();
	return 0;
}

int set_bot_line_match(MENU *lib_menu, int row, int col, char *s)
{
	clean_bottom_line(row,col); refresh();
	mvwprintw(stdscr,row-1,0,"m/%s",s);
	if (set_menu_pattern(lib_menu,s) == E_NO_MATCH) {
		attron(A_BOLD);
		mvwprintw(stdscr,row-1,strlen(s)+4,"(not found)");
		attroff(A_BOLD);
	}
	refresh();
	return 0;
}

int set_fullsearch_buffer(MENU *lib_menu, char *s, int row, int col)
{
	s[0] = '\0';
	echo();
	clean_bottom_line(row,col); refresh();
	char searchstr[] = "Search for:  ";
	mvprintw(row-1,0,"%s",searchstr);
	mvgetnstr(row-1,strlen(searchstr),s,MAX_REGEXP_LEN-1);
	clean_bottom_line(row,col); refresh();
	noecho();
	refresh();
	return 0;
}

int set_pattern_buffer(MENU *lib_menu,char *s, int row, int col)
{
	s[0] = '\0';
	echo();
	clean_bottom_line(row,col); refresh();
	char searchstr[] = "Search for:  ";
	mvprintw(row-1,0,"%s",searchstr);
	mvscanw(row-1,strlen(searchstr),"%11s",s);
	clean_bottom_line(row,col); refresh();
	noecho();
	set_bot_line_match(lib_menu,row,col,s);
	refresh();
	return 0;
}

int frame_menu_help(int row, int col)
{
	print_top_menu_help(stdscr,row,col);
	print_bot_menu_help(stdscr,row,col);
	return 0;
}

int frame_detail_screen(int row, int col, int recordnum)
{
	print_top_details(stdscr,row,col,recordnum);
	print_bot_details(stdscr,row,col);
	return 0;
}

int frame_main_screen(int numrecs, int row, int col)
{
	print_top_line(stdscr, row, col, numrecs);
	refresh();
	print_bot_line(stdscr, row, col);
	refresh();
	return 0;
}

int print_top_menu_help(WINDOW *win, int row, int col)
{
	attron(A_BOLD);
	attron(COLOR_PAIR(1));
	highlight_line(win,0,col);
	attron(A_REVERSE);
	mvwprintw(win,0,0,"--minlib, v1.0");
	print_center(win,0,"Menu View Help");
	print_right(win,0,"Menu View--");
	wmove(win,row,col-1);
	attroff(COLOR_PAIR(1));
	attroff(A_REVERSE | A_BOLD);
	return 0;
}

int print_bot_menu_help(WINDOW *win, int row, int col)
{
	attron(COLOR_PAIR(2));
	attron(A_BOLD);
	highlight_line(win,row-2,col);
	attron(A_REVERSE | A_BOLD);
	mvwprintw(win,row-2,0,"q:back");
	attroff(A_REVERSE | A_BOLD);
	attroff(COLOR_PAIR(2));
	return 0;
}

int print_top_details(WINDOW *win, int row, int col, int recordnum)
{
	char *heading;
	int numdigs;

	attron(A_REVERSE | A_BOLD);
	attron(COLOR_PAIR(1));
	highlight_line(win,0,col);
	attroff(A_REVERSE | A_BOLD);
	numdigs = num_digs(recordnum);
	heading = malloc((numdigs + 2 + 10) * sizeof(char));
	sprintf(heading,"(Record %d)",recordnum);
	attron(A_REVERSE | A_BOLD);
	mvwprintw(win,0,0,"--minlib, v1.0");
	print_center(win,0,heading);
	print_right(win,0,"Detailed View--");
	wmove(win,row,col-1);
	attroff(A_REVERSE | A_BOLD);
	free(heading);
	attroff(COLOR_PAIR(1));
	return 0;
}

int print_bot_details(WINDOW *win, int row, int col)
{
	attron(COLOR_PAIR(2));
	attron(A_BOLD);
	highlight_line(win,row-2,col);
	attron(A_REVERSE);
	mvwprintw(win,row-2,0,"q:back  o:open  c:cover  s:mail  :=shell");
	attroff(A_REVERSE | A_BOLD);
	attroff(COLOR_PAIR(2));
	return 0;
}

int print_top_line(WINDOW *win, int row, int col, int numrecs)
{
	char *heading;
	int numdigs;

	attron(COLOR_PAIR(1));
	attron(A_REVERSE | A_BOLD);
	highlight_line(win,0,col);
	attroff(A_REVERSE | A_BOLD);
	numdigs = num_digs(numrecs);
	heading = malloc((numdigs + 2 + 9) * sizeof(char));
	sprintf(heading,"(%d records)",numrecs);
	attron(A_REVERSE | A_BOLD);
	mvwprintw(win,0,0,"--minlib, v1.0");
	print_center(win,0,heading);
	print_right(win,0,"Menu View--");
	wmove(win,row,col-1);
	attroff(A_REVERSE | A_BOLD);
	attroff(COLOR_PAIR(1));
	free(heading);
	return 0;
}

int print_bottom_open(char *s)
{
	int row, col;

	getmaxyx(stdscr,row,col);
	clean_bottom_line(row, col);
	mvprintw(row-1,0,"Opening %s...",s);
	return 0;
}

int print_generic_bottom_line(char *s)
{
	int row, col;

	getmaxyx(stdscr,row,col);
	clean_bottom_line(row, col);
	mvprintw(row-1,0,"%s...",s);
	return 0;
}

int print_bot_line(WINDOW *win, int row, int col)
{
	attron(COLOR_PAIR(2));
	attron(A_BOLD);
	highlight_line(win,row-2,col);
	attron(A_REVERSE | A_BOLD);
	mvwprintw(win,row-2,0,"q:quit  m:match  /:search r:reload");
	attroff(A_REVERSE | A_BOLD);
	attroff(COLOR_PAIR(2));
	return 0;
}

int print_center(WINDOW *win, int row, char *s)
{
	int len, y, col;

	len = strlen(s);
	getmaxyx(win,y,col);
	mvwprintw(win,row,col/2-len/2,"%s",s);
	return y;
}

int print_right(WINDOW *win, int row, char *s)
{
	int len, y, col;

	len = strlen(s);
	getmaxyx(win,y,col);
	mvwprintw(win,row,col-len,"%s",s);
	return y;
}

int highlight_line(WINDOW *win, int row, int col)
{
	int i;
	attron(A_REVERSE);
	for (i = 0; i < col; ++i)
		mvwprintw(win,row,i," ");
	attroff(A_REVERSE);
	return 0;
}

/* return number of wrapped lines, 0 if all on one */
int wrap_print(WINDOW *win,char *s, int cols, int row)
{
	int len; int left; int numchars;
	char t[2*cols];
	char *ptr;
	int numline = 0;
	int numbytes;

	len = new_strlen(s);
	left = cols - 24 - 2;
	if (len < left) {
		mvwprintw(win,row,24,"%s",s);
		return 0;
	}
	if (len >= left) {
		numchars = len;
		ptr = s;
		while (numchars > 0) {
			numbytes = new_strncpy(t,ptr,left);
			ptr += numbytes;
			numchars -= left;
			mvwprintw(win,row++,24,"%s",t);
			numline++;
		}
		return (int) len / left;
	}
	return 0;
}

int initialize_colors(struct options *globopts)
{
	start_color();
	init_pair(1,get_col_int((globopts+TOP_BACK_COLOR)->optval),
		get_col_int((globopts+TOP_FORE_COLOR)->optval));
	init_pair(2,get_col_int((globopts+BOT_BACK_COLOR)->optval),
		get_col_int((globopts+BOT_FORE_COLOR)->optval));
	init_pair(3,get_col_int((globopts+MEN_FORE_COLOR)->optval),
		get_col_int((globopts+MEN_BACK_COLOR)->optval));
	init_pair(4,get_col_int((globopts+DET_FIELD_FORE_COLOR)->optval),
		get_col_int((globopts+DET_FIELD_BACK_COLOR)->optval));
	init_pair(5,get_col_int((globopts+DET_TXT_FORE_COLOR)->optval),
		get_col_int((globopts+DET_TXT_BACK_COLOR)->optval));
	init_pair(6,get_col_int((globopts+DET_BACK_COLOR)->optval),
		get_col_int((globopts+DET_BACK_COLOR)->optval));
	return 0;
}

int get_col_int(char *s)
{
	int colint;

	if (!strcmp(s,"COLOR_BLACK"))
		colint = 0;
	if (!strcmp(s,"COLOR_RED"))
		colint = 1;
	if (!strcmp(s,"COLOR_GREEN"))
		colint = 2;
	if (!strcmp(s,"COLOR_YELLOW"))
		colint = 3;
	if (!strcmp(s,"COLOR_BLUE"))
		colint = 4;
	if (!strcmp(s,"COLOR_MAGENTA"))
		colint = 5;
	if (!strcmp(s,"COLOR_CYAN"))
		colint = 6;
	if (!strcmp(s,"COLOR_WHITE"))
		colint = 7;
	return colint;
}

int get_number()
{
	char d;
	char num[6];
	int i = 0;

	while ((i < 5) && ((d = getch()) != 10)) {
		switch (d) {
		case '0': case '1': case '2': case '3': case '4': case '5':
		case '6': case '7': case '8': case '9':
			num[i++] = d;
			break;
		}
	}
	num[i] = '\0';
	return atoi(num);
}

/* return 0 if file can be sent; 1 if not */
int send_path(char *s, struct options *globopts, char **ptr, int sel)
{
	char *t = NULL;
	char fn[] = "/tmp/minlib-XXXXXX";
	int fd;
	int i;
	int k = 0;
	char title[19];

	if ((fd = mkstemp(fn)) == -1) {
		fprintf(stderr,"minlib:  error:  cannot save "
				"record data for inclusion in email; "
				"continuing without it");
	}
	for (i=sel+1; *(ptr+i) != NULL && !strstr(*(ptr+i),"%%"); ++i) {
		if (k == 0) {
			if ((strcmp("PATH",*(ptr+i)) == 0) ||
					(strcmp("COVER",*(ptr+i)) == 0)) {
				k = -2;
				continue;
			}
			sprintf(title,"%-18s",*(ptr+i));
			write(fd,title,strlen(title));
			++k;
		} else if (k == 1) {
			write(fd,*(ptr+i),strlen(*(ptr+i)));
			write(fd,"\n",1);
			k = 0;
		} else if (k < 0) {
			++k;
		}
	}
	if ((t = malloc((strlen((globopts+MAIL_SENDER)->optval)
	+ strlen(s) + 28) * sizeof(char))) == NULL) {
		fprintf(stderr,"minlib:  insufficient memory to "
		"store the command line for sending this file\n");
		exit(INSUFF_INTERNAL_MEMORY);
	}
	t[0] = '\0';
	sprintf(t,(globopts+MAIL_SENDER)->optval,s,fn);
	system(t);
	free(t);
	return 0;
}

/* return 0 if file can be opened; 1 if not */
int open_path(char *s, struct options *globopts)
{
	int len;
	int i;
	char ext[12];
	int viewer;
	char *t = NULL;

	len = strlen(s) - 1;
	for (i = len; (s[i] != '.') && (i > 0); --i);
	if (i != 0) {
		strncpy(ext,s+i,11);
		if (!strcmp(ext,".pdf")) {
			viewer = PDF_VIEWER;
		} else if (!strcmp(ext,".html")) {
			viewer = HTML_VIEWER;
		} else if (!strcmp(ext,".epub")) {
			viewer = EPUB_VIEWER;
		} else  if (!strcmp(ext,".ogv") || !strcmp(ext,".mkv") ||
				!strcmp(ext,".mp4")) {
			viewer = OGV_VIEWER;
		} else  if ((!strcmp(ext,".ogg")) || (!strcmp(ext,".spx")) ||
				(!strcmp(ext,".opus"))) {
			viewer = OGG_VIEWER;
		} else  if (!strcmp(ext,".mp3")) {
			viewer = MP3_VIEWER;
		} else  if (!strcmp(ext,".dvi")) {
			viewer = DVI_VIEWER;
		} else  if (!strcmp(ext,".ps")) {
			viewer = PS_VIEWER;
		} else if ((!strcmp(ext,".doc")) || (!strcmp(ext,".ppt")) ||
					(!strcmp(ext,".xls")) || (!strcmp(ext,".docx")) ||
					(!strcmp(ext,".pptx")) || (!strcmp(ext,".xlsx")) ||
					(!strcmp(ext,".odt")) || (!strcmp(ext,".odf")) ||
					(!strcmp(ext,".ods")) || (!strcmp(ext,".odp"))) {
			viewer = OFFICE_VIEWER;
		} else if ((!strcmp(ext,".jpg")) || (!strcmp(ext,".jpeg")) ||
					(!strcmp(ext,".png")) || (!strcmp(ext,".gif")) ||
					(!strcmp(ext,".pnm")) || (!strcmp(ext,".tif")) ||
					(!strcmp(ext,".tiff")) || (!strcmp(ext,".bmp"))) {
			viewer = IMG_VIEWER;
		} else { /* FFF */
			print_generic_bottom_line("Error:  don't know how to open "
					"that file");
			return 1;
		}
		if ((t = malloc((strlen((globopts+viewer)->optval)
		+ strlen(s) + 28) * sizeof(char))) == NULL) {
			fprintf(stderr,"minlib:  insufficient memory to "
			"store the command line for opening this file\n");
			exit(INSUFF_INTERNAL_MEMORY);
		}
		t[0] = '\0';
		sprintf(t,(globopts+viewer)->optval,s);
		strcat(t," > /dev/null 2>&1 &");
		system(t);
		free(t);
		return 0;
	}
	return 1;
}

/*
 * +AMDG
 */
/*
 * This program file was begun on August X, 1206, the feast
 * of St. Lawrence, deacon and martyr, and it is humbly
 * dedicated to him and to the Immaculate Heart of Mary, for
 * their prayers; and to the Sacred Heart of Jesus, for His
 * mercy.
 */

/* to get getline() defined */
#define _POSIX_C_SOURCE 200809L

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#include "utility.h"
#include "errcodes.h"

#define MAX_ICON_NAME_LEN	16	/* maximum length of an icon label */
#define MAX_ICON_CHARS 6 /* max bytes in UTF-8 glyph */

int build_defaults(void);
int validate_icon_line(char *s);

static struct icontype {
	char name[MAX_ICON_NAME_LEN];
	char icon[8];
} *icontypes;

int build_icontypes(void)
{
	int i, j, k = 0;
	int preflen = 0;
	FILE *fp;
	char *line = NULL;
	ssize_t read; size_t len = 0;
	char *iconfile;
	char *home;
	char deficonname[] = "/.config/minlib/iconlist";
	char cwd[3] = "./";

	home = getenv("HOME");
	if (home == NULL) {
		preflen = 3;
	} else {
		preflen = strlen(home) + 1;
	}
	if ((iconfile = malloc((preflen+strlen(deficonname) + 1) * 
				sizeof(char))) == NULL) {
		fprintf(stderr,"minlib:  insufficient memory to "
		"store the name of the icon file\n");
		exit(INSUFF_INTERNAL_MEMORY);
	}
	iconfile[0] = '\0';
	if (home == NULL) {
		strcat(iconfile,cwd); strcat(iconfile,deficonname);
	} else {
		strcat(iconfile,home); strcat(iconfile,deficonname);
	}
	if ((fp = fopen(iconfile,"r")) == NULL) {
		fprintf(stderr,"minlib:  error opening file \"%s\", "
			"with the following error:\n\t%d: %s; using "
			"defaults...\n",iconfile,errno,strerror(errno));
		build_defaults();
		free(iconfile);
		return 0;
	} else {
		if ((icontypes = malloc(2 * sizeof(struct icontype))) == NULL) {
			fprintf(stderr,"minlib:  insufficient memory to "
			"store the list of icon types\n");
			exit(INSUFF_INTERNAL_MEMORY);
		}
		icontypes[0].name[0] = '\0';
		icontypes[0].icon[0] = '\0';
		j = 0;
		while ((read = getline(&line,&len,fp)) != -1) {
			if (validate_icon_line(line) != 0) {
				chomp(line);
				fprintf(stderr,"minlib:  error:  icon type line "
						"\"%s\" is malformed; skipping...\n",line);
				continue;
			}
			if ((icontypes = realloc(icontypes,(j + 1) * 
							sizeof(struct icontype))) == NULL) {
				fprintf(stderr,"minlib:  insufficient memory to "
				"store the list of icon types\n");
				exit(INSUFF_INTERNAL_MEMORY);
			}
			icontypes[j].name[0] = '\0';
			icontypes[j].icon[0] = '\0';
			for (i = 0; line[i] != '\t'; ++i) {
				if (i < MAX_ICON_NAME_LEN)
					icontypes[j].name[i] = line[i];
			}
			icontypes[j].name[i] = '\0';
			++i;
			for (i = i; line[i] != '\0' && line[i] != '\n'; ++i) {
				if (k <= MAX_ICON_CHARS) {
					icontypes[j].icon[k] = line[i];
					++k;
				} else {
					break;
				}
			}
			icontypes[j].icon[k] = '\0';
			++j; k = 0;
		}
		icontypes[j-1].name[0] = '\0';
		free(line);
	}
	free(iconfile);
	fclose(fp);
	return 0;
}

/* return 0 if good, non-zero if error */
int validate_icon_line(char *s)
{
	int i;
	char istab = 0;

	for (i = 0; s[i] != '\0'; ++i) {
		if (s[i] == '\t')
			istab = i;
	}
	if (istab == 0)
		return 1;
	if (istab == (i-1))
		return 1;
	return 0;
}

int build_defaults(void)
{
	int i;

	if ((icontypes = malloc(5 * sizeof(struct icontype))) == NULL) {
		fprintf(stderr,"minlib:  insufficient memory to "
		"store the list of icon types\n");
		exit(INSUFF_INTERNAL_MEMORY);
	}
	for (i = 0; i < 5; ++i) {
		icontypes[i].name[0] = '\0';
		icontypes[i].icon[0] = '\0';
	}
	strcat(icontypes[0].name,"book");
	strcat(icontypes[0].icon,"🕮");
	strcat(icontypes[1].name,"film");
	strcat(icontypes[1].icon,"🎞");
	strcat(icontypes[2].name,"music");
	strcat(icontypes[2].icon,"𝅘𝅥");
	icontypes[3].name[0] = '\0';
	strcat(icontypes[3].icon,"?");
	return 0;
}

int make_icon(char *s, char *t)
{
	int i;

	for (i = 0; icontypes[i].name[0] != '\0'; ++i) {
		if (!strcmp(icontypes[i].name,s)) {
			new_strcat(t,icontypes[i].icon,5);
			return 0;
		}
	}
	new_strcat(t,icontypes[i].icon,5);
	return 0;
}

int free_iconlist(void)
{
	free(icontypes);
	return 0;
}

#+AMDG
#
#This document was begun on 9 May 1200, the feast of St.
#Gregory Nazianzen, ECD, and it is humbly dedicated to
#him, to St.  Wulfric of Haselbury, and to the Immaculate
#Heart of Mary for their prayers, and to the Sacred Heart
#of Jesus for His mercy.
.POSIX: # for POSIX compat; must be first non-comment line
SHELL=/bin/sh
.SUFFIXES:
VERSION=1.0
CC=gcc
INCLUDEDIR=/usr/local/include/
CFLAGS=-I$(INCLUDEDIR) -Wall -g -std=c99 -pedantic
OBJ=utility.o read_optfile.o readlib.o format_recs.o \
	gui.o full_search.o add_files.o stats.o icon_type.o
CURSLIB=-lform -lmenuw -lncursesw
EXTLIB=-lextractor
PREFIX=/usr/local
BINFILE=minlib
MANFILE=minlib.1

all : minlib

minlib :	main.c $(OBJ) errcodes.h options.h
	$(CC) $(CFLAGS) -o minlib main.c $(OBJ) $(CURSLIB) $(EXTLIB)

gui.o : gui.c errcodes.h options.h full_search.h utility.h
	$(CC) $(CFLAGS) -c gui.c

icon_type.o : icon_type.c icon_type.h utility.h errcodes.h
	$(CC) $(CFLAGS) -c icon_type.c

full_search.o : full_search.c errcodes.h utility.h
	$(CC) $(CFLAGS) -c full_search.c

stats.o : stats.c errcodes.h
	$(CC) $(CFLAGS) -c stats.c

format_recs.o : format_recs.c utility.c errcodes.h icon_type.h
	$(CC) $(CFLAGS) -c format_recs.c

readlib.o : readlib.c utility.c errcodes.h
	$(CC) $(CFLAGS) -c readlib.c

read_optfile.o : read_optfile.c errcodes.h utility.h options.h
	$(CC) $(CFLAGS) -c read_optfile.c

add_files.o : add_files.c utility.h errcodes.h
	$(CC) $(CFLAGS) -c add_files.c

utility.o : utility.c utility.h errcodes.h
	$(CC) $(CFLAGS) -c utility.c

.PHONY: install
install :
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	mkdir -p $(DESTDIR)$(PREFIX)/share/man/man1
	cp -f $(BINFILE) $(DESTDIR)$(PREFIX)/bin
	cp -f $(MANFILE) $(DESTDIR)$(PREFIX)/share/man/man1/

.PHONY: uninstall
uninstall :
	rm $(DESTDIR)$(PREFIX)/bin/$(BINFILE)
	rm $(DESTDIR)$(PREFIX)/share/man/man1/$(MANFILE)

tarball :
	tar -czvvf minlib-$(VERSION).tar.gz *.c *.h Makefile minlib.1

doc :
	groff -man -Tascii -a minlib.1 > minlib_man.txt;
	groff -man -Thtml minlib.1 > minlib_man.html;

.PHONY : clean
clean :
	-rm $(OBJ) $(BINFILE)

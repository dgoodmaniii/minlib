/*
 * +AMDG
 */
/*
 * This program file was begun on August X, 1206, the feast
 * of St. Lawrence, deacon and martyr, and it is humbly
 * dedicated to him and to the Immaculate Heart of Mary, for
 * their prayers; and to the Sacred Heart of Jesus, for His
 * mercy.
 */

int build_icontypes(void);
int free_iconlist(void);
int make_icon(char *s, char *t);

/*
 * +AMDG
 */
/*
 * This document was begun on 1X May 1200, Holy Trinity
 * Sunday, and it is humbly dedicated to the Father, the
 * Son, and the Holy Spirit, for their mercy.
 */


/* to get getline() defined */
#define _XOPEN_SOURCE 700

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<errno.h>
#include<ftw.h>
#include<unistd.h>
#include<limits.h>
#include<sys/stat.h>
#include<extractor.h>
#include"errcodes.h"
#include"add_files.h"

char **hptr = NULL;

/* function that libextractor passes its data to */
int print_metadata(void *cls,const char *plugin_name, enum
	EXTRACTOR_MetaType type, enum EXTRACTOR_MetaFormat format,
	const char *data_mime_type,const char *data, size_t
	data_len);

/* returns 0 on success, 1 on failure */
/* addap is 0 to add, 1 to append */
int add_files(char *dirname,char addapp,char **ptr)
{
	int status;
	struct stat st_buf;

	if (sizeof(ptr) != 0) {
		hptr = ptr;
	}
	status = stat(dirname,&st_buf);
	if (status != 0) {
		fprintf(stderr,"minlib:  couldn't get status of \"%s\", "
		"with error number %d:\n\t%s",dirname,errno,strerror(errno));
		return BAD_ADDED_FILE;
	}
	if (S_ISREG(st_buf.st_mode))
		add_file(dirname);
	else if (S_ISDIR(st_buf.st_mode))
		add_dir(dirname);
	return 0;
}

/* return 0 if already in system; 1 if not */
int already_added(char *s)
{
	int i;

	if (hptr == NULL)
		return 1;
	for (i = 0; *(hptr+i) != NULL; ++i) {
		if (strstr(*(hptr+i),"PATH")) {
			if (strcmp(*(hptr+i+1),s) == 0)
				return 0;
		}
	}
	return 1;
}

int add_file(char *s)
{
	if (already_added(s) == 0) {
		fprintf(stderr,"File \"%s\" already in database; skipping...\n",s);
		return 0;
	}
	if (strstr(s,".pdf")) {
		fprintf(stderr,"Processing %s...\n",s);
		pdf_metadata(s);
	} else if (strstr(s,".epub")) {
		fprintf(stderr,"Processing %s...\n",s);
		epub_metadata(s);
	} else if (strstr(s,".dvi") || strstr(s,".flac") ||
	strstr(s,".html") || strstr(s,".midi") ||
	strstr(s,".mpeg") || strstr(s,".odf") || strstr(s,".ogg")
	|| strstr(s,".ps") || strstr(s,".riff") ||
	strstr(s,".wav") || strstr(s,".doc") || strstr(s,".xls")
	|| strstr(s,".ppt") || strstr(s,".sxw") ||
	strstr(s,".mp3") || strstr(s,".s3m") || strstr(s,".nsf")
	|| strstr(s,".sid") || strstr(s,".real") ||
	strstr(s,".flv") || strstr(s,".avi") || strstr(s,".qt")
	|| strstr(s,".asf") || strstr(s,".ogv") || strstr(s,".ogt")) {
		fprintf(stderr,"Processing %s...\n",s);
		multi_metadata(s);
	} else {
		fprintf(stderr,"%s not a minlib format; skipping...\n",s);
	}
	return 0;
}

int descend(const char *fpath, const struct stat *sb,
	int tflag, struct FTW *ftwbuf)
{
	char *s;

	if ((s = malloc((strlen(fpath) + 1) * sizeof(char))) == NULL) {
		fprintf(stderr,"minlib:  insufficient memory to store "
		"the next point of the directory tree\n");
		exit(INSUFF_INTERNAL_MEMORY);
	}
	strncpy(s,fpath,strlen(fpath)+1);
	if (S_ISREG(sb->st_mode))
		add_file(s);
	free(s);
	return 0;
}

/* the following calls add_file for each file it sees */
int add_dir(char *s)
{
	if (nftw(s,descend,20,0) != 0)
		;
	return 0;
}


int epub_metadata(char *s)
{
	int i, j;
	char *line = NULL;
	ssize_t read; size_t len = 0;
	FILE *p;
	char *tmpptr;
	char *t;
	char fhead[MAX_FIELD_LEN+1];
	char text[MAX_TITLE_LEN+1];
	char actpath[PATH_MAX+1];
	int ind, oind;

	if ((t = malloc((strlen(s) + 36) * sizeof(char))) == NULL) {
		fprintf(stderr,"minlib:  insufficient memory to store "
		"command necessary to extra epub metadata\n");
		exit(INSUFF_INTERNAL_MEMORY);
	}
	*t = '\0';
	printf("%%%%\n");
	strcat(t,"unzip -c \"");
	strcat(t,s); strcat(t,"\"");
	strcat(t," *content.opf");
	if ((p = popen(t,"r")) == NULL) {
		fprintf(stderr,"minlib:  error opening file \"%s\" "
		"with error code %d:\n\t%s\n",s,errno,strerror(errno));
		exit(ERROR_OPEN_ADDFILE);
	}
	while ((read = getline(&line,&len,p)) != -1) {
		if ((tmpptr = strstr(line,"dc:"))) {
			ind = 0;
			for (i = 3; tmpptr[i] != '\0' && isalpha(tmpptr[i]) &&
					ind < MAX_FIELD_LEN; ++i) {
				fhead[ind++] = tmpptr[i];
			}
			fhead[ind] = '\0';
			if (!strcmp(fhead,"contributor")) {
				if (strstr(line,"edt"))
					strncpy(fhead,"EDITOR",MAX_FIELD_LEN);
				else if (strstr(line,"trl"))
					strncpy(fhead,"TRANS",MAX_FIELD_LEN);
				else if (strstr(line,"adp"))
					strncpy(fhead,"ADAPTER",MAX_FIELD_LEN);
				else if (strstr(line,"arr"))
					strncpy(fhead,"ARRANGER",MAX_FIELD_LEN);
				else if (strstr(line,"art"))
					strncpy(fhead,"ARTIST",MAX_FIELD_LEN);
				else if (strstr(line,"aqt"))
					strncpy(fhead,"QUOTEAUTH",MAX_FIELD_LEN);
				else if (strstr(line,"aft"))
					strncpy(fhead,"AFTERWORD",MAX_FIELD_LEN);
				else if (strstr(line,"aui"))
					strncpy(fhead,"INTRO",MAX_FIELD_LEN);
				else if (strstr(line,"ant"))
					strncpy(fhead,"ANTEC.",MAX_FIELD_LEN);
				else if (strstr(line,"bkp"))
					strncpy(fhead,"PRODUCER",MAX_FIELD_LEN);
				else if (strstr(line,"clb"))
					strncpy(fhead,"COLLAB.",MAX_FIELD_LEN);
				else if (strstr(line,"cmm"))
					strncpy(fhead,"COMM.OR",MAX_FIELD_LEN);
				else if (strstr(line,"dsr"))
					strncpy(fhead,"DESIGNER",MAX_FIELD_LEN);
				else if (strstr(line,"ill"))
					strncpy(fhead,"ILLUS.OR",MAX_FIELD_LEN);
				else if (strstr(line,"lyr"))
					strncpy(fhead,"LYRICIST",MAX_FIELD_LEN);
				else if (strstr(line,"mus"))
					strncpy(fhead,"MUSICIAN",MAX_FIELD_LEN);
				else if (strstr(line,"nar"))
					strncpy(fhead,"NARRATOR",MAX_FIELD_LEN);
				else if (strstr(line,"pht"))
					strncpy(fhead,"PHOTO.ER",MAX_FIELD_LEN);
				else if (strstr(line,"prt"))
					strncpy(fhead,"PRINTER",MAX_FIELD_LEN);
				else if (strstr(line,"red"))
					strncpy(fhead,"REDACTOR",MAX_FIELD_LEN);
				else if (strstr(line,"rev"))
					strncpy(fhead,"REVIEWER",MAX_FIELD_LEN);
				else if (strstr(line,"spn"))
					strncpy(fhead,"SPONSOR",MAX_FIELD_LEN);
				else if (strstr(line,"ths"))
					strncpy(fhead,"ADVISOR",MAX_FIELD_LEN);
				else if (strstr(line,"trc"))
					strncpy(fhead,"TRANSCR.ER",MAX_FIELD_LEN);
				else
					strncpy(fhead,"OTHER",MAX_FIELD_LEN);
			}
			if (!strcmp(fhead,"identifier")) {
				if (strstr(line,"ISBN"))
					strncpy(fhead,"ISBN",MAX_FIELD_LEN);
			}
			if (!strcmp(fhead,"creator"))
				strncpy(fhead,"AUTHOR",MAX_FIELD_LEN);
			if (!strcmp(fhead,"AUTHOR") || !strcmp(fhead,"title")
			|| !strcmp(fhead,"language") || !strcmp(fhead,"subject")
			|| !strcmp(fhead,"rights") || !strcmp(fhead,"contributor")
			|| !strcmp(fhead,"EDITOR") ||  !strcmp(fhead,"source")
			|| !strcmp(fhead,"TRANS") || !strcmp(fhead,"ISBN")
			|| !strcmp(fhead,"description") || !strcmp(fhead,"publisher")
			|| !strcmp(fhead,"ADAPTER") || !strcmp(fhead,"ARRANGER")
			|| !strcmp(fhead,"ARTIST") || !strcmp(fhead,"QUOTEAUTH")
			|| !strcmp(fhead,"AFTERWORD") || !strcmp(fhead,"INTRO")
			|| !strcmp(fhead,"ANTEC.") || !strcmp(fhead,"PRODUCER")
			|| !strcmp(fhead,"COLLAB.") || !strcmp(fhead,"COMM.OR")
			|| !strcmp(fhead,"DESIGNER") || !strcmp(fhead,"ILLUS.OR")
			|| !strcmp(fhead,"LYRICIST") || !strcmp(fhead,"MUSICIAN")
			|| !strcmp(fhead,"NARRATOR") || !strcmp(fhead,"PHOTO.ER")
			|| !strcmp(fhead,"PRINTER") || !strcmp(fhead,"REDACTOR")
			|| !strcmp(fhead,"REVIEWER") || !strcmp(fhead,"SPONSOR")
			|| !strcmp(fhead,"ADVISOR") || !strcmp(fhead,"TRANSCR.ER")
			|| !strcmp(fhead,"OTHER") || !strcmp(fhead,"TRANSCR.ER")
			|| !strcmp(fhead,"date") || !strcmp(fhead,"type")
			|| !strcmp(fhead,"format") || !strcmp(fhead,"relation")
			|| !strcmp(fhead,"coverage")) {
				while(line[i++] != '>');
				oind = 0;
				while((line[i] != '<') && (line[i] != '\0')) {
					text[oind++] = line[i++];
					if (oind >= MAX_TITLE_LEN)
						break;
				}
				text[oind] = '\0';
				for (j = 0; fhead[j] != '\0'; ++j)
					printf("%c",toupper(fhead[j]));
				printf(":\t%s\n",text);
				fhead[0] = '\0'; text[0] = '\0';
			}
		}
	}
	printf("PATH:\t%s\n",realpath(s,actpath));
	free(t);
	free(line);
	pclose(p);
	return 0;
}

int pdf_metadata(char *s)
{
	int i;
	char *line = NULL;
	ssize_t read; size_t len = 0;
	FILE *p;
	char *tmpptr;
	char *t;
	char fhead[MAX_FIELD_LEN+1];
	char text[MAX_TITLE_LEN+1];
	char actpath[PATH_MAX+1];
	int ind;

	if ((t = malloc((strlen(s) + 26) * sizeof(char))) == NULL) {
		fprintf(stderr,"minlib:  insufficient memory to store "
		"command necessary to extra pdf metadata\n");
		exit(INSUFF_INTERNAL_MEMORY);
	}
	*t = '\0';
	printf("%%%%\n");
	strcat(t,"pdfinfo \""); strcat(t,s); strcat(t,"\"");
	if ((p = popen(t,"r")) == NULL) {
		fprintf(stderr,"minlib:  error opening file \"%s\" "
		"with error code %d:\n\t%s\n",s,errno,strerror(errno));
		exit(ERROR_OPEN_ADDFILE);
	}
	fhead[0] = '\0'; text[0] = '\0';
	while ((read = getline(&line,&len,p)) != -1) {
		if (strstr(line,"Title:"))
			strncpy(fhead,"TITLE",MAX_FIELD_LEN);
		if (strstr(line,"Author:"))
			strncpy(fhead,"AUTHOR",MAX_FIELD_LEN);
		if (strstr(line,"Subject:"))
			strncpy(fhead,"SUBJECT",MAX_FIELD_LEN);
		if (strstr(line,"Keywords:"))
			strncpy(fhead,"KEYWORDS",MAX_FIELD_LEN);
		if (strstr(line,"Producer:"))
			strncpy(fhead,"PRODUCER",MAX_FIELD_LEN);
		if (strstr(line,"CreationDate:"))
			strncpy(fhead,"CREATIONDATE",MAX_FIELD_LEN);
		if (strstr(line,"ModDate:"))
			strncpy(fhead,"MODDATE",MAX_FIELD_LEN);
		if (strstr(line,"Tagged:"))
			strncpy(fhead,"TAGGED",MAX_FIELD_LEN);
		if (strstr(line,"Encrypted:"))
			strncpy(fhead,"ENCRYPTED",MAX_FIELD_LEN);
		if (strstr(line,"Page size:"))
			strncpy(fhead,"PAGESIZE",MAX_FIELD_LEN);
		if (strstr(line,"Page rot:"))
			strncpy(fhead,"PAGEROT",MAX_FIELD_LEN);
		if (strstr(line,"PDF version:"))
			strncpy(fhead,"PDFVERSION",MAX_FIELD_LEN);
		if (strstr(line,"Creator:"))
			strncpy(fhead,"CREATOR",MAX_FIELD_LEN);
		if (fhead[0] != '\0') {
			tmpptr = (strstr(line,":")); ++tmpptr;
			while(isspace(*(tmpptr))) ++tmpptr;
			ind = 0;
			for (i = 0; tmpptr[i] != '\n' && tmpptr[i] != '\0' && ind < MAX_TITLE_LEN; ++i)
				text[ind++] = tmpptr[i];
			text[ind] = '\0';
			printf("%s:\t%s\n",fhead,text);
			fhead[0] = '\0'; text[0] = '\0';
		}
	}
	printf("PATH:\t%s\n",realpath(s,actpath));
	free(line);
	pclose(p);
	return 0;
}

int multi_metadata(char *s)
{
	char actpath[PATH_MAX+1];
	struct EXTRACTOR_PluginList *plugins =
		EXTRACTOR_plugin_add_defaults (EXTRACTOR_OPTION_DEFAULT_POLICY);
	printf("%%%%\n");
	EXTRACTOR_extract (plugins, s,
		NULL,0,&print_metadata,stdout);
	EXTRACTOR_plugin_remove_all(plugins);
	printf("PATH:\t%s\n",realpath(s,actpath));
	return 0;
}

int print_metadata(void *cls,const char *plugin_name, enum
EXTRACTOR_MetaType type, enum EXTRACTOR_MetaFormat format,
const char *data_mime_type,const char *data, size_t
data_len)
{
	int i;

	for (i = 0; EXTRACTOR_metatype_to_string(type)[i] != '\0' && i < 12; ++i) {
		printf("%c",toupper(EXTRACTOR_metatype_to_string(type)[i]));
	}
	printf(":  ");
	for (i = 0; data[i] != '\0'; ++i)
		if (data[i] != '\n')
		printf("%c",data[i]);
	printf("\n");
	return 0;
}

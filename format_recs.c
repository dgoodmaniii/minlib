/*
 * +AMDG
 */
/*
 * This document was begun on 9 May 1200, the feast of St.
 * Gregory Nazianzen, ECD, and it is humbly dedicated to
 * him, to St. Wulfric of Haselbury, and to the Immaculate
 * Heart of Mary for their prayers, and to the Sacred Heart
 * of Jesus for His mercy.
*/
/*
 * This file takes the list made from the records in
 * readlib.c and formats the output strings.
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include"icon_type.h"
#include"utility.h"

#define BOOKICON "🕮"  /* a Unicode book icon */
#define FILMICON "🎞"  /* a Unicode film icon */
#define MUSICICON "𝅘𝅥"  /* a Unicode quarter note */
#define OTHERICON "?"

int populate(char **raw, int stind, char letter);
int make_string(char **raw, char **format, char *formstring);

int format_recs(char **ptr, char *formstring, char **formed, int linenums, int *recnums)
{
	int j = -1;

	j = make_string(ptr,formed,formstring);
	quicksort(formed,recnums,j+1);
	return 0;
}

/* allocate memory for the formatted strings and fill them */
int make_string(char **raw, char **format, char *formstring)
{
	int i = 0; int j = 0; int k; int l = 0;
	int recnum = 0;
	int num;
	int corrrec;
	char buf[12];
	char tbuf[12];

	tbuf[0] = '\0';
	for (i = 0; *(raw+i) != NULL; ++i) {
		if (strstr(*(raw+i),"%%")) {
			recnum = atoi(*(raw+i)) - 1;
			++j;
			for (k = 0; *(formstring+k) != '\0'; ++k) {
				if (*(formstring+k) == '%') {
					while (isdigit(*(formstring+(++k)))) {
						if (l < 12)
							buf[l++] = *(formstring+k);
					}
					buf[l] = '\0';
					num = atoi(buf);
					corrrec = populate(raw,i+1,*(formstring+k));
					if (corrrec != 0) {
						if (strstr(*(raw+corrrec-1),"ICONTYPE") != NULL) {
							make_icon(*(raw+corrrec),tbuf);
							new_strcat(*(format+recnum),tbuf,num);
							tbuf[0] = '\0';
						} else {
							new_strcat(*(format+recnum),*(raw+corrrec),num);
						}
					} else {
						new_strcat(*(format+recnum)," ",num);
					}
					l = 0;
				} else {
					new_strcat(*(format+recnum),formstring+k,1);
				}
			}
		}
	}
	return recnum;
}

int populate(char **raw, int stind, char letter)
{
	for (stind = stind; (*(raw+stind) != NULL) && 
	(!strstr(*(raw+stind),"%%"));++stind) {
		if (letter == 't') {
			if (!strcmp(*(raw+stind),"TITLE"))
				return stind+1;
		} else if (letter == 'a') {
			if ((!strcmp(*(raw+stind),"AUTHOR")) ||
			(!strcmp(*(raw+stind),"ARTIST")))
				return stind+1;
		} else if (letter == 'y') {
			if (!strcmp(*(raw+stind),"YEAR"))
				return stind+1;
		} else if (letter == 'D') {
			if (!strcmp(*(raw+stind),"DATE"))
				return stind+1;
		} else if (letter == 'l') {
			if ((!strcmp(*(raw+stind),"LANG")) ||
			(!strcmp(*(raw+stind),"LANGONE")) ||
			(!strcmp(*(raw+stind),"LANGUAGE")))
				return stind+1;
		} else if (letter == 's') { /* secondary language */
			if (!strcmp(*(raw+stind),"LANGTWO"))
				return stind+1;
		} else if (letter == 'g') { /* "g"enre */
			if((!strcmp(*(raw+stind),"TYPE"))||(!strcmp(*(raw+stind),"GENRE")))
				return stind+1;
		} else if (letter == 'p') {
			if (!strcmp(*(raw+stind),"PUBLISHER"))
				return stind+1;
		} else if (letter == 'k') {
			if (!strcmp(*(raw+stind),"KEYWORDS"))
				return stind+1;
		} else if (letter == 'S') {
			if (!strcmp(*(raw+stind),"SUBJECT"))
				return stind+1;
		} else if (letter == 'L') {
			if (!strcmp(*(raw+stind),"AUTHLAST"))
				return stind+1;
		} else if (letter == 'M') {
			if (!strcmp(*(raw+stind),"AUTHMID"))
				return stind+1;
		} else if (letter == 'F') {
			if (!strcmp(*(raw+stind),"AUTHFIRST"))
				return stind+1;
		} else if (letter == 'm') {
			if (!strcmp(*(raw+stind),"MEDIUM"))
				return stind+1;
		} else if (letter == 'P') {
			if ((!strcmp(*(raw+stind),"PLACE")) || 
			(!strcmp(*(raw+stind),"LOCATION")))
				return stind+1;
		} else if (letter == 'e') {
			if (!strcmp(*(raw+stind),"EDITOR"))
				return stind+1;
		} else if (letter == 'T') {
			if (!strcmp(*(raw+stind),"TRANS"))
				return stind+1;
			if (!strcmp(*(raw+stind),"TRANSLATOR"))
				return stind+1;
		} else if (letter == 'd') {
			if (!strcmp(*(raw+stind),"DIRECTOR"))
				return stind+1;
		} else if (letter == 'i') {
			if (!strcmp(*(raw+stind),"ILLUS.OR"))
				return stind+1;
		} else if (letter == 'I') {
			if (!strcmp(*(raw+stind),"ICONTYPE"))
				return stind+1;
		}
	}
	return 0;
}
				
int split_form(char *s)
{
	int i, j = 0;
	char t[4];

	for (i = 0; *(s+i) != '\0'; ++i) {
		if (isdigit(*(s+i)) && (j < 4)) {
			t[j++] = *(s+i);
		}
	}
	t[j] = '\0';
	return atoi(t);
}
